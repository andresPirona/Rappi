/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : sistemaap

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2017-12-12 12:43:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tabejecuciones
-- ----------------------------
DROP TABLE IF EXISTS `tabejecuciones`;
CREATE TABLE `tabejecuciones` (
  `idEjecucion` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) DEFAULT NULL,
  `fechaEje` date DEFAULT NULL,
  `horaIni` time DEFAULT NULL,
  `horaFin` time DEFAULT NULL,
  `duracion` time DEFAULT NULL,
  `idTarea` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEjecucion`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tabejecuciones
-- ----------------------------
INSERT INTO `tabejecuciones` VALUES ('5', '1', '0000-00-00', '13:00:00', '16:00:00', '03:00:00', '1');
INSERT INTO `tabejecuciones` VALUES ('6', '1', '2018-05-05', '13:00:00', '16:00:00', '03:00:00', '1');
INSERT INTO `tabejecuciones` VALUES ('7', '1', '2017-12-12', '13:00:00', '16:00:00', '03:00:00', '1');
INSERT INTO `tabejecuciones` VALUES ('8', '1', '2017-12-12', '13:00:00', '16:00:00', '03:00:00', '1');
INSERT INTO `tabejecuciones` VALUES ('16', '1', '2017-12-12', '13:00:00', '16:00:00', '03:00:00', '1');
INSERT INTO `tabejecuciones` VALUES ('18', '1', '2017-12-12', '15:00:00', '17:00:00', '02:00:00', '1');
INSERT INTO `tabejecuciones` VALUES ('19', '1', '2017-12-12', '15:00:00', '17:00:00', '02:00:00', '3');
INSERT INTO `tabejecuciones` VALUES ('20', '1', '2017-12-12', '11:00:00', '15:00:00', '04:00:00', '1');

-- ----------------------------
-- Table structure for tbltareas
-- ----------------------------
DROP TABLE IF EXISTS `tbltareas`;
CREATE TABLE `tbltareas` (
  `idTarea` int(11) NOT NULL AUTO_INCREMENT,
  `strTarea` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idTarea`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbltareas
-- ----------------------------
INSERT INTO `tbltareas` VALUES ('1', 'Limpiar');
INSERT INTO `tbltareas` VALUES ('2', 'Comprar');
INSERT INTO `tbltareas` VALUES ('3', 'Vender');
INSERT INTO `tbltareas` VALUES ('4', 'Gestionar');
INSERT INTO `tbltareas` VALUES ('5', 'Procesar');

-- ----------------------------
-- Table structure for usertable
-- ----------------------------
DROP TABLE IF EXISTS `usertable`;
CREATE TABLE `usertable` (
  `iduser` int(255) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `tipo` int(255) DEFAULT NULL,
  `idTareas` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usertable
-- ----------------------------
INSERT INTO `usertable` VALUES ('1', 'andres', '1234', '1', '1,2');
INSERT INTO `usertable` VALUES ('2', 'eduardo', '123', null, null);
INSERT INTO `usertable` VALUES ('3', 'eduardo', '123', null, null);
INSERT INTO `usertable` VALUES ('4', 'eduardo', '123', null, null);
INSERT INTO `usertable` VALUES ('9', 'eduard', '12345', null, null);
INSERT INTO `usertable` VALUES ('10', 'andresp', '123', null, null);
SET FOREIGN_KEY_CHECKS=1;
