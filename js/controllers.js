angular.module('app.controllers', ['ngDialog'])
.controller('loginCtrl', function($scope, $rootScope, restServices,  $timeout, ngDialog, $location, $state){

	$scope.user="";
	$scope.pass="";

	$scope.reguser="";
	$scope.regpass="";
	$scope.regpass2="";

	$scope.errorLogin = false;

$scope.openRegistro=function(){
	$('.container').stop().addClass('active');
}

$scope.closeRegistro=function(){
 	$('.container').stop().removeClass('active');
}
///// EVITAMOS CARACTERES ESPECIALES EN CaMPO USER
$('#user').on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});
///// EVITAMOS CARACTERES ESPECIALES EN CaMPO pass
$('#pass').on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});

$scope.loadingLogin = false;
$scope.loadingRegister = false;


			$scope.login = function() {

				$scope.errorLogin = false;

					if ($scope.pass != "" && $scope.user != "" && $scope.pass != undefined && $scope.user != undefined){
				
					$scope.loadingLogin = true;
					$timeout(logueo, 1500);

					} 
					
				
			}

			$scope.register = function() {



					if ($scope.regpass != "" && $scope.reguser != "" && $scope.regpass2 != "" && $scope.regpass != undefined && $scope.reguser != undefined && $scope.regpass2 != undefined){
					

							if ($scope.regpass === $scope.regpass2){

								$scope.loadingRegister = true;
								$timeout(regis, 1500);

							} else {

								ngDialog.open({ template: 'popUps/errorClaves.html', 
									className: 'ngdialog-theme-default' });	

							}
					

					} 
						
			}

			function logueo() {
    		
				restServices.Login($scope.user,$scope.pass).success(function(response) {
		                if(response.estado==1){

		                		
		                		$('#botonLogin').blur();

		                		$rootScope.nombreUser = $scope.user;
		                		$rootScope.idUser = response.user.iduser;

		                		$state.go('panel');

		                } else if(response.estado==2) {
								
								$scope.errorLogin = true;
								$('#botonLogin').blur();

		                }

							$scope.loadingLogin = false;
            	}).error(function () {
                      	alert("Error");
                      
                })

			}

			function regis() {
    		
					$scope.usuario = {
						"user" : $scope.reguser,
						"pass" : $scope.regpass
					};

				restServices.RegistrarUser($scope.usuario).success(function(response) {
		                if(response.estado==1){

		                		ngDialog.open({ template: 'popUps/registroExitoso.html', 
									className: 'ngdialog-theme-default' });	

		                		$scope.reguser="";
		                		$scope.regpass="";
		                		$scope.regpass2="";

		                		$('.container').stop().removeClass('active');

		                		$('#botonRegister').blur();

		                		alert("Registro Exitoso");

		                } else if(response.estado==2) {
								
								alert("Registro Error");
								$('#botonRegister').blur();

		                }

							$scope.loadingRegister = false;
            	}).error(function () {
                      	alert("Error");
                      
                })

			}

			$scope.showAlert = function(ev) {

					 ngDialog.open({ template: 'popUps/error.html'});

  			};




  })
.controller('panelCtrl', function($scope, $rootScope, restServices,  $timeout, ngDialog, $location, $state){


		$scope.abrirRegistro = function(){

				$state.go('registrar');
		};

		$scope.nombre = $rootScope.nombreUser;

		$scope.listado = [];
		$scope.totalUsers = "";
		$scope.totalTareas = "";

		$scope.refresh = function(){

			$scope.listadoTareas();	
			$scope.todosUser();
			$scope.todasTareas();
		}

		$scope.listadoTareas = function(){

			restServices.ListadoTareas().success(function(response) {
		                if(response.estado==1){

		                		$scope.listado = response.tareas;

		                } else if(response.estado==2) {
								
								alert("Listado Error");

		                }

							$scope.loadingRegister = false;
            	}).error(function () {
                      	alert("Error");
                      
                })

		};

		$scope.todosUser = function(){
				
				restServices.UsuariosTodos().success(function(response) {
		                if(response.estado==1){

		                		$scope.totalUsers = response.usuarios;


		                } else if(response.estado==2) {
								
								alert("Error al recuperar total de usuarios Registrados");

		                }

							$scope.loadingRegister = false;
            	}).error(function () {
                      	alert("Error");
                      
                })
		};


		$scope.todasTareas = function(){
				restServices.TareasTodas().success(function(response) {
		                if(response.estado==1){

		                		$scope.totalTareas = response.tareas;

		                } else if(response.estado==2) {
								
								alert("Error al recuperar total de tareas Registradas");

		                }

							$scope.loadingRegister = false;
            	}).error(function () {
                      	alert("Error");
                      
                })
		};

		$scope.listadoTareas();	
			$scope.todosUser();
			$scope.todasTareas();


		$scope.mini = function(e) {
	        e.preventDefault();
	        $('#toggle-btn').toggleClass('active');

	        $('.side-navbar').toggleClass('shrinked');
	        $('.content-inner').toggleClass('active');

	        if ($(window).outerWidth() > 1183) {
	            if ($('#toggle-btn').hasClass('active')) {
	                $('.navbar-header .brand-small').hide();
	                $('.navbar-header .brand-big').show();
	            } else {
	                $('.navbar-header .brand-small').show();
	                $('.navbar-header .brand-big').hide();
	            }
	        }

	        if ($(window).outerWidth() < 1183) {
	            $('.navbar-header .brand-small').show();
	        }
		}

})
.controller('registroCtrl', function($scope, $rootScope, restServices,  $timeout, ngDialog, $location, $state){


		$scope.nombre = $rootScope.nombreUser;


		$scope.tareasArray = "";
		$scope.comboTareas = [];

		////alert("idUser="+$rootScope.idUser);

		$scope.listadoTareasUser = function(){

			restServices.ListadoTareasUser($scope.tareasArray).success(function(response) {
		                if(response.estado==1){

		                		$scope.comboTareas = response.user;

		                } else if(response.estado==2) {
								
								alert("Listado Error");

		                }

            	}).error(function () {
                      	alert("Error");
                      
                })

		};

		$scope.getUser = function(){

			restServices.getUser($rootScope.idUser).success(function(response) {
		                if(response.estado==1){

//////1er elemento Unico
		                		$scope.tareasArray = response.user[0].idTareas;

		                		$scope.listadoTareasUser();

		                } else if(response.estado==2) {
								
								alert("Datos User Error");

		                }
            	}).error(function () {
                      	alert("Error");
                      
                })

		};

		$scope.getUser();



		$(function(){
		  $('#calcular').click(function(){

		  	var inicio = $('#inicio').val();
		  	var fin = $('#fin').val();

		  	var combo = $('#comboTarea').val();

		  	if (inicio === "" || fin ==="" || inicio === null || fin ===null || inicio === undefined || fin ===undefined){
		  		alert("Los tiempos (INICIO y FIN) no pueden estar vacios");
		  	} else {

		  		var start = moment(inicio, 'H:mm:ss');
    			var end = moment(fin, 'H:mm:ss');

		  		var duracion = moment.duration(end - start);
				var str = moment(duracion._data).format("HH:mm");
			    $('#duracion').val(str);

		  	}

		   
		  });
		});


		$scope.abrirListado = function(){

				$state.go('panel');
		};

		$scope.registrarTarea = function(){


			////verificacion de campos
			var tarea = $('#comboTarea').val();
			var inicio = $('#inicio').val();
			var fin = $('#fin').val();
			var duracion = $('#duracion').val();



			if (tarea == "" || tarea == null || tarea == undefined || tarea == "0"){
				alert("Escoja la tarea a Registrar");
				$('#comboTarea').focus();
				return false;
			}

			if (inicio == "" || inicio == null || inicio == undefined || inicio == "0"){
				alert("Escoja la hora de Inicio");
				$('#inicio').focus();
				return false;
			}

			if (fin == "" || fin == null || fin == undefined || fin == "0"){
				alert("Escoja la hora de Finalización");
				$('#fin').focus();
				return false;
			}

			if (duracion == "" || duracion == null || duracion == undefined || duracion == "0"){
				alert("Calcule la Diferencia entre las horas");
				$('#duracion').focus();
				return false;
			}





				$scope.tarea= {
					"idTarea":""+tarea,
					"idUser":$rootScope.idUser,
					"horaIni":inicio,
					"horaFin":fin,
					"duracion":duracion
				} 

				restServices.RegistrarTarea($scope.tarea).success(function(response) {
		                if(response.estado==1){

		                		alert("Tarea Registrada Exitosamente");


								$state.go('panel');


		                } else if(response.estado==2) {
								
								alert("Error al Registrar Tarea");

		                }

					
            	}).error(function () {
                      	alert("Error");
                      
                });

    


				
		};


})