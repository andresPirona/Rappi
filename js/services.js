angular.module('app.services', [])
.service('restServices', ['$http', function($http){

    var apiUrl = 'http://localhost/andresPrueba/restApi/';

    ///////////NUEVOS
    this.Login = function (user, pass){
        return $http({
            method: 'GET',
            url: apiUrl+'/inicio_sesion.php?user='+user+'&pass='+pass});
    };

    this.ListadoTareas = function (){
        return $http({
            method: 'GET',
            url: apiUrl+'/ObtenerTareas.php'});
    };

    this.ListadoTareasUser = function (tareas){
        return $http({
            method: 'GET',
            url: apiUrl+'/ObtenerTareasUser.php?tareas='+tareas});
    };

    this.getUser = function (iduser){
        return $http({
            method: 'GET',
            url: apiUrl+'/ObtenerUserbyId.php?iduser='+iduser});
    };

    this.UsuariosTodos = function (){
        return $http({
            method: 'GET',
            url: apiUrl+'/ObtenerUsuariosTodos.php'});
    };

    this.TareasTodas = function (){
        return $http({
            method: 'GET',
            url: apiUrl+'/ObtenerTareasTodas.php'});
    };


    this.RegistrarUser = function(usuario){
        return $http({
            method: 'POST',
            url: apiUrl+'/insert_user.php',
            data: usuario});
    };

    this.RegistrarTarea = function(tarea){
        return $http({
            method: 'POST',
            url: apiUrl+'/insert_tarea.php',
            data: tarea});
    };
    ///////////LOCALES
}]);
