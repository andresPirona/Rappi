angular.module('app.routes', ['ui.router'])

.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

$urlRouterProvider.otherwise('/');

        $stateProvider
            .state('login', {
                url: '/',
                templateUrl: 'vistas/login.html',
                controller:'loginCtrl'
            })
            .state('panel', {
                url: '/panel',
                templateUrl: 'vistas/panel.html',
                controller:'panelCtrl'
            })
            .state('registrar', {
                url: '/registrar',
                templateUrl: 'vistas/registrar.html',
                controller:'registroCtrl'
            });

            $locationProvider
  .html5Mode(true);

});
