<?php
/**
 * Obtiene el detalle de una meta especificada por
 * su identificador "idMeta"
 */

require 'Usuarios.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    if (isset($_GET['tareas'])) {

        // Obtener parámetro idMeta
        $tareas = $_GET['tareas'];

        // Tratar consulta
        $consulta = Usuarios::getAllTareasUser($tareas);


        if ($consulta) {

            $combo["estado"] = "1";
            $combo["user"] = $consulta;
            // Enviar objeto json de la meta
            print json_encode($combo);
        } else {
            // Enviar respuesta de error general
            print json_encode(
                array(
                    'estado' => '2',
                    'mensaje' => 'No se obtuvo el registro'
                )
            );
        }

    } else {
        // Enviar respuesta de error
        print json_encode(
            array(
                'estado' => '3',
                'mensaje' => 'Se necesita un identificador'
            )
        );
    }
}
?>