<?php
/**
 * Obtiene el detalle de una meta especificada por
 * su identificador "idMeta"
 */

require 'Usuarios.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    if (isset($_GET['user'], $_GET['pass'])) {

        // Obtener parámetro idMeta
        $user = $_GET['user'];
        $pass = $_GET['pass'];

        // Tratar consulta
        $consulta = Usuarios::IniciarSesion($user, $pass);


        if ($consulta) {

            $usuario["estado"] = "1";
            $usuario["user"] = $consulta;
            // Enviar objeto json de la meta
            print json_encode($usuario);
        } else {
            // Enviar respuesta de error general
            print json_encode(
                array(
                    'estado' => '2',
                    'mensaje' => 'No se obtuvo el registro'
                )
            );
        }

    } else {
        // Enviar respuesta de error
        print json_encode(
            array(
                'estado' => '3',
                'mensaje' => 'Se necesita un identificador'
            )
        );
    }
}
?>