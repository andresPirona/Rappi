<?php

/**
 * Representa el la estructura de las metas
 * almacenadas en la base de datos
 */
require 'Database.php';

class Usuarios
{
    function __construct()
    {
    }

    /**
     * Retorna en la fila especificada de la tabla 'meta'
     *
     * @param $idMeta Identificador del registro
     * @return array Datos del registro
     */

/////////////////////////////////PRINCIPALES


    public static function IniciarSesion($user, $pass)
    {
        // Consulta de la meta
        $consulta = "SELECT iduser, user, tipo FROM userTable WHERE user = ? AND pass = ?";

        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute(array($user, $pass));
            // Capturar primera fila del resultado
            $row = $comando->fetch(PDO::FETCH_ASSOC);
            return $row;

        } catch (PDOException $e) {
            // Aquí puedes clasificar el error dependiendo de la excepción
            // para presentarlo en la respuesta Json
            return -1;
        }
    }







    /////////////////////////////GET FUNCTIONS
    

    public static function getAll()
    {
        $consulta = "SELECT idEjecucion, idUser, fechaEje, horaIni, horaFin, duracion, (SELECT strTarea from tblTareas WHERE idTarea = tareas.idTarea) as nombreTarea, (SELECT user from userTable WHERE iduser = tareas.idUser) as nombreUser FROM tabEjecuciones as tareas ORDER BY tareas.idEjecucion Desc";
        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();

            return $comando->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            return false;
        }
    }

    public static function getAllUsuarios()
    {
        $consulta = "SELECT * FROM usertable";
        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();

            return $comando->rowCount();

        } catch (PDOException $e) {
            return false;
        }
    }

    public static function getAllTareas()
    {
        $consulta = "SELECT * FROM tabEjecuciones";
        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();

            return $comando->rowCount();

        } catch (PDOException $e) {
            return false;
        }
    }

        public static function getAllTareasUser($tareas)
    {
        // Consulta de la meta
        $consulta = "SELECT * FROM tblTareas WHERE idTarea IN (".$tareas.")";

        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute(array($tareas));
            // Capturar primera fila del resultado
     
             return $comando->fetchAll(PDO::FETCH_ASSOC);


        } catch (PDOException $e) {
            // Aquí puedes clasificar el error dependiendo de la excepción
            // para presentarlo en la respuesta Json
            return -1;
        }
    }

        public static function getUserbyId($iduser)
    {
        // Consulta de la meta
        $consulta = "SELECT * FROM usertable WHERE iduser = ?";

        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute(array($iduser));
            // Capturar primera fila del resultado
     
             return $comando->fetchAll(PDO::FETCH_ASSOC);


        } catch (PDOException $e) {
            // Aquí puedes clasificar el error dependiendo de la excepción
            // para presentarlo en la respuesta Json
            return -1;
        }
    }


     

    /////////////////////////////////////////INSERT FUNCTIONS

    public static function insertNewUser(
        $user,
        $pass
    )
    {
        // Sentencia INSERT
        $comando = "INSERT INTO usertable ( " .
            "user," .
            " pass)" .
            " VALUES(?,?)";

        // Preparar la sentencia
        $sentencia = Database::getInstance()->getDb()->prepare($comando);

        return $sentencia->execute(
            array(
                    $user,
                    $pass
            )
        );

    }

     public static function insertNewTarea(
        $idTarea,
        $idUser,
        $horaIni,
        $horaFin,
        $duracion
    )
    {
        // Sentencia INSERT
        $comando = "INSERT INTO tabejecuciones ( " .
            "idTarea," .
            "idUser," .
            "fechaEje," .
            "horaIni," .
            "horaFin," .
            " duracion)" .
            " VALUES(?,?,Now(),?,?,?)";

        // Preparar la sentencia
        $sentencia = Database::getInstance()->getDb()->prepare($comando);

        return $sentencia->execute(
            array(
                    $idTarea,
                    $idUser,
                    $horaIni,
                    $horaFin,
                    $duracion
            )
        );

    }


    ///////////////////////////////////UPDATES FUNCTIONS

    public static function updateGps(
        $lat,
        $long,
        $iduser
    )
    {
        date_default_timezone_set('Etc/GMT+4');
        $date = date('Y-m-d H:i:s');
        // Creando consulta UPDATE
        $consulta = "UPDATE tbl_gps" .
            " SET latitud=?, longitud=? , fecha=?" .
            "WHERE iduser=?";

        // Preparar la sentencia
        $cmd = Database::getInstance()->getDb()->prepare($consulta);

        // Relacionar y ejecutar la sentencia
        $cmd->execute(array($lat, $long, $date, $iduser));

        return $cmd;
    }

        public static function updateCantinicio(
        $iduser
    )
    {
        // Creando consulta UPDATE
        $consulta = "UPDATE tbl_user" .
            " SET cantinicio=1 " .
            "WHERE iduser=?";

        // Preparar la sentencia
        $cmd = Database::getInstance()->getDb()->prepare($consulta);

        // Relacionar y ejecutar la sentencia
        $cmd->execute(array($iduser));

        return $cmd;
    }

     public static function updateFotoPerfilIdUser($imgperfil, $iduser)
    {
        // Creando consulta UPDATE
        $consulta = "UPDATE tbl_user" .
            " SET imgperfil=? " .
            "WHERE iduser=?";

        // Preparar la sentencia
        $cmd = Database::getInstance()->getDb()->prepare($consulta);

        // Relacionar y ejecutar la sentencia
        $cmd->execute(array($imgperfil, $iduser));

        return $cmd;
    }

}

?>